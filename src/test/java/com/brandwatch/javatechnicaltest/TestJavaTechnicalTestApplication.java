package com.brandwatch.javatechnicaltest;

import com.brandwatch.JavaTechnicalTestApplication;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.testcontainers.service.connection.ServiceConnection;
import org.springframework.context.annotation.Bean;
import org.testcontainers.containers.PostgreSQLContainer;

@TestConfiguration(proxyBeanMethods = false)
public class TestJavaTechnicalTestApplication {

	@Bean
	@ServiceConnection
	PostgreSQLContainer<?> postgresContainer() {
		return new PostgreSQLContainer<>("postgres:alpine3.18");
	}

	public static void main(String[] args) {
		SpringApplication.from(JavaTechnicalTestApplication::main).with(TestJavaTechnicalTestApplication.class).run(args);
	}

}
