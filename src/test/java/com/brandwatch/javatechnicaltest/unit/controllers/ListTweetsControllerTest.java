package com.brandwatch.javatechnicaltest.unit.controllers;

import com.brandwatch.controllers.impl.ListTweetsControllerImpl;
import com.brandwatch.dtos.TweetDTO;
import com.brandwatch.mappers.TweetMapper;
import com.brandwatch.model.Tweet;
import com.brandwatch.services.ListTweetsService;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ListTweetsControllerTest {

  @Mock
  private ListTweetsService listTweetsService;

  @Mock
  private TweetMapper tweetMapper;

  @InjectMocks
  private ListTweetsControllerImpl listTweetsController;

  @Test
  @DisplayName("Successfully called service and  mapped tweet")
  void listTweetsTest() {
    // Given
    LocalDateTime start = LocalDateTime.parse("2023-08-08T20:55:55.138");
    LocalDateTime end = LocalDateTime.parse("2023-08-08T22:35:55.138");
    Tweet tweet = Tweet.builder()
            .id(UUID.randomUUID())
            .content("Content")
            .timestamp(LocalDateTime.parse("2023-08-08T21:55:55.138"))
            .build();
    List<Tweet> listOfTweets = List.of(tweet);
    TweetDTO tweetDTO = TweetDTO.builder()
            .id(UUID.randomUUID())
            .content("Content")
            .timestamp(LocalDateTime.parse("2023-08-08T21:55:55.138"))
            .build();
    List<TweetDTO> listOfTweetDTOs = List.of(tweetDTO);

    // When
    when(listTweetsService.listTweets(start, end))
            .thenReturn(listOfTweets);
    when(tweetMapper.convertModelToDTO(listOfTweets))
            .thenReturn(listOfTweetDTOs);

    // Then
    List<TweetDTO> result = listTweetsController.listTweets(start, end);

    // Assert
    assertNotNull(result);
    assertEquals(tweetDTO.getId(), result.get(0).getId());
    assertEquals(tweetDTO.getContent(), result.get(0).getContent());
    assertEquals(tweetDTO.getTimestamp(), result.get(0).getTimestamp());
  }

}
