package com.brandwatch.javatechnicaltest.unit.controllers;

import com.brandwatch.controllers.impl.CreateTweetControllerImpl;
import com.brandwatch.dtos.TweetDTO;
import com.brandwatch.mappers.TweetMapper;
import com.brandwatch.model.Tweet;
import com.brandwatch.services.CreateTweetService;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDateTime;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class CreateTweetControllerTest {

  @Mock
  private CreateTweetService createTweetService;

  @Mock
  private TweetMapper tweetMapper;

  @InjectMocks
  private CreateTweetControllerImpl createTweetController;

  @Test
  @DisplayName("Successfully mapped tweet and called service")
  void createTweetTest() {
    // Given
    Tweet tweetToCreate = Tweet.builder()
            .content("Content")
            .timestamp(LocalDateTime.parse("2023-08-08T21:55:55.138"))
            .build();
    TweetDTO tweetToCreateDTO = TweetDTO.builder()
            .content("Content")
            .timestamp(LocalDateTime.parse("2023-08-08T21:55:55.138"))
            .build();
    Tweet createdTweet = Tweet.builder()
            .id(UUID.randomUUID())
            .content("Content")
            .timestamp(LocalDateTime.parse("2023-08-08T21:55:55.138"))
            .build();
    TweetDTO createdTweetDTO = TweetDTO.builder()
            .id(UUID.randomUUID())
            .content("Content")
            .timestamp(LocalDateTime.parse("2023-08-08T21:55:55.138"))
            .build();

    // When
    when(tweetMapper.convertDTOToModel(tweetToCreateDTO))
            .thenReturn(tweetToCreate);
    when(createTweetService.createTweet(tweetToCreate))
            .thenReturn(createdTweet);
    when(tweetMapper.convertModelToDTO(createdTweet))
            .thenReturn(createdTweetDTO);

    // Then
    TweetDTO result = createTweetController.createTweet(tweetToCreateDTO);

    // Assert
    assertNotNull(result);
    assertEquals(createdTweetDTO.getContent(), result.getContent());
    assertEquals(createdTweetDTO.getTimestamp(), result.getTimestamp());
  }

}
