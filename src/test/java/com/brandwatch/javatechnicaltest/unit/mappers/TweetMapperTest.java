package com.brandwatch.javatechnicaltest.unit.mappers;

import com.brandwatch.dtos.TweetDTO;
import com.brandwatch.mappers.impl.TweetMapperImpl;
import com.brandwatch.model.Tweet;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@ExtendWith(MockitoExtension.class)
class TweetMapperTest {

  @InjectMocks
  private TweetMapperImpl tweetMapper;

  @Test
  @DisplayName("Successfully converted tweet DTO to tweet model")
  void convertTweetDtoToModelTest() {
    // Given
    TweetDTO tweetDTO = TweetDTO.builder()
            .id(UUID.randomUUID())
            .content("Content")
            .timestamp(LocalDateTime.parse("2023-08-08T21:55:55.138"))
            .build();

    // When

    // Then
    Tweet result = tweetMapper.convertDTOToModel(tweetDTO);

    // Assert
    assertNotNull(result);
    assertEquals(tweetDTO.getId(), result.getId());
    assertEquals(tweetDTO.getContent(), result.getContent());
    assertEquals(tweetDTO.getTimestamp(), result.getTimestamp());
  }

  @Test
  @DisplayName("Successfully converted tweet model to tweet DTO")
  void convertTweetModelToDTOTest() {
    // Given
    Tweet tweet = Tweet.builder()
            .id(UUID.randomUUID())
            .content("Content")
            .timestamp(LocalDateTime.parse("2023-08-08T21:55:55.138"))
            .build();

    // When

    // Then
    TweetDTO result = tweetMapper.convertModelToDTO(tweet);

    // Assert
    assertNotNull(result);
    assertEquals(tweet.getId(), result.getId());
    assertEquals(tweet.getContent(), result.getContent());
    assertEquals(tweet.getTimestamp(), result.getTimestamp());
  }

  @Test
  @DisplayName("Successfully converted list of tweet DTOs to tweet models")
  void convertListOfTweetDTOsToModelsTest() {
    // Given
    TweetDTO tweetDTO = TweetDTO.builder()
            .id(UUID.randomUUID())
            .content("Content")
            .timestamp(LocalDateTime.parse("2023-08-08T21:55:55.138"))
            .build();
    List<TweetDTO> tweetDTOs = List.of(tweetDTO);

    // When

    // Then
    List<Tweet> result = tweetMapper.convertDTOToModel(tweetDTOs);

    // Assert
    assertNotNull(result);
    assertEquals(tweetDTO.getId(), result.get(0).getId());
    assertEquals(tweetDTO.getContent(), result.get(0).getContent());
    assertEquals(tweetDTO.getTimestamp(), result.get(0).getTimestamp());
  }

  @Test
  @DisplayName("Successfully converted list of tweet models to tweet DTOs")
  void convertListOfTweetModelsToDTOsTest() {
    // Given
    Tweet tweet = Tweet.builder()
            .id(UUID.randomUUID())
            .content("Content")
            .timestamp(LocalDateTime.parse("2023-08-08T21:55:55.138"))
            .build();
    List<Tweet> tweets = List.of(tweet);

    // When

    // Then
    List<TweetDTO> result = tweetMapper.convertModelToDTO(tweets);

    // Assert
    assertNotNull(result);
    assertEquals(tweet.getId(), result.get(0).getId());
    assertEquals(tweet.getContent(), result.get(0).getContent());
    assertEquals(tweet.getTimestamp(), result.get(0).getTimestamp());
  }

}
