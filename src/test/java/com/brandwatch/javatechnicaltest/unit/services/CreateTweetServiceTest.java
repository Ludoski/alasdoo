package com.brandwatch.javatechnicaltest.unit.services;

import com.brandwatch.model.Tweet;
import com.brandwatch.repository.TweetRepository;
import com.brandwatch.services.impl.CreateTweetServiceImpl;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDateTime;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class CreateTweetServiceTest {

  @Mock
  private TweetRepository tweetRepositoryMock;

  @InjectMocks
  private CreateTweetServiceImpl createTweetService;

  @Test
  @DisplayName("Successfully created tweet")
  void createTweetTest() {
    // Given
    Tweet tweet = Tweet.builder()
            .id(UUID.randomUUID())
            .content("Content")
            .timestamp(LocalDateTime.parse("2023-08-08T21:55:55.138"))
            .build();

    // When
    when(tweetRepositoryMock.save(tweet))
            .thenReturn(tweet);

    // Then
    Tweet result = createTweetService.createTweet(tweet);

    // Assert
    assertNotNull(result);
    assertEquals(tweet, result);
  }

}
