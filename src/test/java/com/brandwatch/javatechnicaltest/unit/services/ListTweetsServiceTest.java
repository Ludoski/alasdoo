package com.brandwatch.javatechnicaltest.unit.services;

import com.brandwatch.exceptions.InvalidTimeRangeException;
import com.brandwatch.model.Tweet;
import com.brandwatch.repository.TweetRepository;
import com.brandwatch.services.impl.ListTweetsServiceImpl;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class ListTweetsServiceTest {

  @Mock
  private TweetRepository tweetRepositoryMock;

  @InjectMocks
  private ListTweetsServiceImpl listTweetsService;

  @Test
  @DisplayName("Successfully listed tweets")
  void listTweetsTest() {
    // Given
    LocalDateTime start = LocalDateTime.parse("2023-08-08T20:55:55.138");
    LocalDateTime end = LocalDateTime.parse("2023-08-08T22:35:55.138");
    Tweet tweet = Tweet.builder()
            .id(UUID.randomUUID())
            .content("Content")
            .timestamp(LocalDateTime.parse("2023-08-08T21:55:55.138"))
            .build();
    Iterable<Tweet> iterableTweets = List.of(tweet);

    // When
    when(tweetRepositoryMock.findByTimestampBetween(start, end))
            .thenReturn(iterableTweets);

    // Then
    List<Tweet> result = listTweetsService.listTweets(start, end);

    // Assert
    assertNotNull(result);
    assertEquals(tweet, result.get(0));
  }

  @Test
  @DisplayName("No tweets found")
  void noTweetsFoundTest() {
    // Given
    LocalDateTime start = LocalDateTime.parse("2023-08-08T20:55:55.138");
    LocalDateTime end = LocalDateTime.parse("2023-08-08T22:35:55.138");
    Iterable<Tweet> iterableTweets = Collections.emptyList();

    // When
    when(tweetRepositoryMock.findByTimestampBetween(start, end))
            .thenReturn(iterableTweets);

    // Then
    List<Tweet> result = listTweetsService.listTweets(start, end);

    // Assert
    assertNotNull(result);
    assertEquals(Collections.emptyList(), result);
  }

  @Test
  @DisplayName("Invalid time range")
  void invalidTimeRangeTest() {
    // Given
    LocalDateTime start = LocalDateTime.parse("2023-08-08T22:35:55.138");
    LocalDateTime end = LocalDateTime.parse("2023-08-08T20:55:55.138");

    // Then
    assertThatThrownBy(() -> listTweetsService.listTweets(start, end))
            .isInstanceOf(InvalidTimeRangeException.class);
  }

}
