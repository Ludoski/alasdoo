package com.brandwatch.javatechnicaltest.integration;

import com.brandwatch.dtos.TweetDTO;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;

import java.time.LocalDateTime;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

class CreateTweetTest extends BaseIntegrationTest {

  @Test
  @DisplayName("200 - Create tweet")
  void createTweetTest() throws Exception {
    // Given
    TweetDTO tweetToCreateDTO = TweetDTO.builder()
            .content("Content")
            .timestamp(LocalDateTime.now())
            .build();

    // When

    // Then
    mockMvc.perform(
                    post("/tweets")
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(objectMapper.writeValueAsString(tweetToCreateDTO))
                            .accept(MediaType.APPLICATION_JSON)
            )
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.status").value(HttpStatus.OK.getReasonPhrase()))
            .andExpect(jsonPath("$.message").isNotEmpty());
  }

  @Test
  @DisplayName("400 - Invalid content null")
  void invalidContentNullTest() throws Exception {
    // Given
    TweetDTO tweetToCreateDTO = TweetDTO.builder()
            .timestamp(LocalDateTime.now())
            .build();

    // When

    // Then
    mockMvc.perform(
                    post("/tweets")
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(objectMapper.writeValueAsString(tweetToCreateDTO))
                            .accept(MediaType.APPLICATION_JSON)
            )
            .andExpect(status().isBadRequest());
  }

  @Test
  @DisplayName("400 - Invalid content blank")
  void invalidContentBlankTest() throws Exception {
    // Given
    TweetDTO tweetToCreateDTO = TweetDTO.builder()
            .content("")
            .timestamp(LocalDateTime.now())
            .build();

    // When

    // Then
    mockMvc.perform(
                    post("/tweets")
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(objectMapper.writeValueAsString(tweetToCreateDTO))
                            .accept(MediaType.APPLICATION_JSON)
            )
            .andExpect(status().isBadRequest());
  }

  @Test
  @DisplayName("400 - Invalid timestamp null")
  void invalidTimestampNullTest() throws Exception {
    // Given
    TweetDTO tweetToCreateDTO = TweetDTO.builder()
            .content("Content")
            .build();

    // When

    // Then
    mockMvc.perform(
                    post("/tweets")
                            .contentType(MediaType.APPLICATION_JSON)
                            .content(objectMapper.writeValueAsString(tweetToCreateDTO))
                            .accept(MediaType.APPLICATION_JSON)
            )
            .andExpect(status().isBadRequest());
  }

}
