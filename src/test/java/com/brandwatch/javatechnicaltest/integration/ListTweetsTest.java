package com.brandwatch.javatechnicaltest.integration;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;

import java.time.LocalDateTime;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

class ListTweetsTest extends BaseIntegrationTest {

  @Test
  @DisplayName("200 - List tweets")
  void listTweetsTest() throws Exception {
    // Given
    LocalDateTime start = LocalDateTime.parse("2023-08-08T20:55:55.138");
    LocalDateTime end = LocalDateTime.parse("2023-08-08T22:35:55.138");

    // When

    // Then
    mockMvc.perform(
                    get("/tweets")
                            .param("start", String.valueOf(start))
                            .param("end", String.valueOf(end))
                            .accept(MediaType.APPLICATION_JSON)
            )
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.status").value(HttpStatus.OK.getReasonPhrase()))
            .andExpect(jsonPath("$.message").isNotEmpty());
  }

  @Test
  @DisplayName("400 - Invalid time range")
  void invalidTimeRangeTest() throws Exception {
    // Given
    LocalDateTime start = LocalDateTime.parse("2023-08-08T22:35:55.138");
    LocalDateTime end = LocalDateTime.parse("2023-08-08T20:55:55.138");

    // When

    // Then
    mockMvc.perform(
                    get("/tweets")
                            .param("start", String.valueOf(start))
                            .param("end", String.valueOf(end))
                            .accept(MediaType.APPLICATION_JSON)
            )
            .andExpect(status().isBadRequest());
  }

  @Test
  @DisplayName("400 - Invalid start time")
  void invalidStartTimeTest() throws Exception {
    // Given
    LocalDateTime end = LocalDateTime.parse("2023-08-08T20:55:55.138");

    // When

    // Then
    mockMvc.perform(
                    get("/tweets")
                            .param("end", String.valueOf(end))
                            .accept(MediaType.APPLICATION_JSON)
            )
            .andExpect(status().isBadRequest());
  }

  @Test
  @DisplayName("400 - Invalid end time")
  void invalidEndTimeTest() throws Exception {
    // Given
    LocalDateTime start = LocalDateTime.parse("2023-08-08T22:35:55.138");

    // When

    // Then
    mockMvc.perform(
                    get("/tweets")
                            .param("start", String.valueOf(start))
                            .accept(MediaType.APPLICATION_JSON)
            )
            .andExpect(status().isBadRequest());
  }

}
