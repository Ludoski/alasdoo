package com.brandwatch.repository;

import com.brandwatch.model.Tweet;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.UUID;

/**
 * Tweet repository
 */
@Repository
public interface TweetRepository extends CrudRepository<Tweet, UUID> {

  /**
   * Find tweets in time range
   * @param start     Start time
   * @param end       End time
   * @return          Iterable of tweets
   */
  Iterable<Tweet> findByTimestampBetween(LocalDateTime start, LocalDateTime end);

}
