package com.brandwatch.services;

import com.brandwatch.model.Tweet;

/**
 * Create tweet service
 */
public interface CreateTweetService {

  /**
   * Create tweet
   * @param tweet       Tweet to save
   * @return            Saved tweet
   */
  Tweet createTweet(Tweet tweet);

}
