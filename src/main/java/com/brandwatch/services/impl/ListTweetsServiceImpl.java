package com.brandwatch.services.impl;

import com.brandwatch.exceptions.InvalidTimeRangeException;
import com.brandwatch.model.Tweet;
import com.brandwatch.repository.TweetRepository;
import com.brandwatch.services.ListTweetsService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * @see ListTweetsService
 */
@Slf4j
@Service
public class ListTweetsServiceImpl implements ListTweetsService {

  @Autowired
  private TweetRepository tweetRepository;

  /**
   * @see ListTweetsService#listTweets(LocalDateTime, LocalDateTime)
   */
  @Override
  public List<Tweet> listTweets(LocalDateTime start, LocalDateTime end) {
    // Check for invalid time range
    if (end.isBefore(start)) {
      log.error("Invalid time range while listing tweets - start = {}, end = {}.", start, end);
      throw new InvalidTimeRangeException();
    }

    // Get tweets from database
    Iterable<Tweet> iterableTweets = tweetRepository.findByTimestampBetween(start, end);

    // Map iterable to list
    List<Tweet> tweets = new ArrayList<>();
    iterableTweets.forEach(tweets::add);

    return tweets;
  }

}
