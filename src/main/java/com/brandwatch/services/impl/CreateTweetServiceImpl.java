package com.brandwatch.services.impl;

import com.brandwatch.model.Tweet;
import com.brandwatch.repository.TweetRepository;
import com.brandwatch.services.CreateTweetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @see CreateTweetService
 */
@Service
public class CreateTweetServiceImpl implements CreateTweetService {

  @Autowired
  private TweetRepository tweetRepository;

  /**
   * @see CreateTweetService#createTweet(Tweet) 
   */
  @Override
  public Tweet createTweet(Tweet tweet) {
    return tweetRepository.save(tweet);
  }

}
