package com.brandwatch.services;

import com.brandwatch.model.Tweet;

import java.time.LocalDateTime;
import java.util.List;

/**
 * List tweets service
 */
public interface ListTweetsService {

  /**
   * List tweets in time range
   * @param start     Start time
   * @param end       End time
   * @return          List of tweets in time range
   */
  List<Tweet> listTweets(LocalDateTime start, LocalDateTime end);

}
