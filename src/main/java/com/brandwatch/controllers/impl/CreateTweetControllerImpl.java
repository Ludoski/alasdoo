package com.brandwatch.controllers.impl;

import com.brandwatch.controllers.CreateTweetController;
import com.brandwatch.dtos.TweetDTO;
import com.brandwatch.mappers.TweetMapper;
import com.brandwatch.model.Tweet;
import com.brandwatch.services.CreateTweetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

/**
 * @see CreateTweetController
 */
@RestController
public class CreateTweetControllerImpl implements CreateTweetController {

  @Autowired
  private CreateTweetService createTweetService;

  @Autowired
  private TweetMapper tweetMapper;

  /**
   * @see CreateTweetController#createTweet(TweetDTO) 
   */
  @Override
  public TweetDTO createTweet(TweetDTO tweetDTO) {
    Tweet tweet = tweetMapper.convertDTOToModel(tweetDTO);
    return tweetMapper.convertModelToDTO(createTweetService.createTweet(tweet));
  }

}
