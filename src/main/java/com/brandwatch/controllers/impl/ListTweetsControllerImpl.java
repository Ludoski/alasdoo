package com.brandwatch.controllers.impl;

import com.brandwatch.controllers.ListTweetsController;
import com.brandwatch.dtos.TweetDTO;
import com.brandwatch.mappers.TweetMapper;
import com.brandwatch.services.ListTweetsService;
import jakarta.validation.constraints.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @see ListTweetsController
 */
@RestController
public class ListTweetsControllerImpl implements ListTweetsController {

    @Autowired
    private ListTweetsService listTweetsService;

    @Autowired
    private TweetMapper tweetMapper;

    /**
     * @see ListTweetsController#listTweets(LocalDateTime, LocalDateTime)
     */
    @Override
    public List<TweetDTO> listTweets(@NotNull LocalDateTime start, @NotNull LocalDateTime end) {
        return tweetMapper.convertModelToDTO(listTweetsService.listTweets(start, end));
    }

}
