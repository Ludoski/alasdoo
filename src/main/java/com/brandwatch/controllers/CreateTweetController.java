package com.brandwatch.controllers;

import com.brandwatch.dtos.TweetDTO;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Create tweet controller
 */
public interface CreateTweetController {

  /**
   * Create tweet endpoint
   * @param tweetDTO    Tweet to save
   * @return            Saved tweet
   */
  @PostMapping("/tweets")
  @ResponseBody
  TweetDTO createTweet(@RequestBody @Validated TweetDTO tweetDTO);

}
