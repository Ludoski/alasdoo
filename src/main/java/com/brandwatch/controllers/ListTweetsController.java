package com.brandwatch.controllers;

import com.brandwatch.dtos.TweetDTO;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.time.LocalDateTime;
import java.util.List;

/**
 * List tweets controller
 */
public interface ListTweetsController {

  /**
   * List tweets in time range
   * @param start     Start time
   * @param end       End time
   * @return          List of tweets in time range
   */
  @GetMapping("/tweets")
  @ResponseBody
  List<TweetDTO> listTweets(
          @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) @NotBlank @NotNull @RequestParam LocalDateTime start,
          @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) @NotBlank @NotNull @RequestParam LocalDateTime end);

}
