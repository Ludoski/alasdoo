package com.brandwatch.exceptions;

import org.springframework.http.HttpStatus;

/**
 * Invalid time range exception
 */
public class InvalidTimeRangeException extends ApplicationException {

  private static final String RESPONSE_MESSAGE = "Invalid time range.";
  private static final HttpStatus HTTP_STATUS = HttpStatus.BAD_REQUEST;

  public InvalidTimeRangeException() {
    super(RESPONSE_MESSAGE, HTTP_STATUS);
  }

}
