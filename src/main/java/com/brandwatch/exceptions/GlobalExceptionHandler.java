package com.brandwatch.exceptions;

import com.brandwatch.dtos.response.ResponseBody;
import jakarta.validation.UnexpectedTypeException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.Map;

/**
 * Global exception handler that packs custom response for client according to exception
 */
@RestControllerAdvice
public class GlobalExceptionHandler {

  /**
   * Handle custom application exceptions
   * @param exception     Custom application exception
   * @return              Response message
   */
  @ExceptionHandler(ApplicationException.class)
  public ResponseEntity<Map<String, Object>> handle(ApplicationException exception) {
    HttpStatus status = exception.getHttpStatus();
    String message = exception.getResponseMessage();
    return createResponse(status, message);
  }

  /**
   * Handle MissingServletRequestParameterException
   * @param exception     Exception
   * @return              Response message
   */
  @ExceptionHandler(MissingServletRequestParameterException.class)
  public ResponseEntity<Map<String, Object>> handle(MissingServletRequestParameterException exception) {
    HttpStatus status = HttpStatus.BAD_REQUEST;
    String message = exception.getMessage();
    return createResponse(status, message);
  }

  /**
   * Handle UnexpectedTypeException
   * @param exception     Exception
   * @return              Response message
   */
  @ExceptionHandler(UnexpectedTypeException.class)
  public ResponseEntity<Map<String, Object>> handle(UnexpectedTypeException exception) {
    HttpStatus status = HttpStatus.BAD_REQUEST;
    String message = exception.getMessage();
    return createResponse(status, message);
  }

  /**
   * Handle MethodArgumentNotValidException
   * @param exception     Exception
   * @return              Response message
   */
  @ExceptionHandler(MethodArgumentNotValidException.class)
  public ResponseEntity<Map<String, Object>> handle(MethodArgumentNotValidException exception) {
    HttpStatus status = HttpStatus.BAD_REQUEST;
    String message = exception.getMessage();
    return createResponse(status, message);
  }

  /**
   * Handle all the other exceptions
   * @param exception     Exception
   * @return              Response message
   */
  @ExceptionHandler(Exception.class)
  public ResponseEntity<Map<String, Object>> handle(Exception exception) {
    HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;
    String message = exception.getClass().toString();
    return createResponse(status, message);
  }

  /**
   * Method for packing custom http response
   * @param status      Http response status
   * @param message     Http response message
   * @return            Response message
   */
  private ResponseEntity<Map<String, Object>> createResponse(HttpStatus status, String message) {
    Map<String, Object> body = new ResponseBody(status, message).toMap();
    return new ResponseEntity<>(body, status);
  }

}
