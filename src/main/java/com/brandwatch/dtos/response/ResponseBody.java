package com.brandwatch.dtos.response;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpStatus;

import java.util.Map;

/**
 * Custom response body
 */
@NoArgsConstructor
public class ResponseBody {

  @JsonIgnore
  private final ObjectMapper objectMapper = new ObjectMapper();

  @JsonProperty
  private HttpStatus status;

  @JsonProperty
  private Object message;

  public ResponseBody(HttpStatus status, Object message) {
    this.status = status;
    this.message = message;
  }

  public HttpStatus getStatus() {
    return status;
  }

  public Object getMessage() {
    return message;
  }

  public Map<String, Object> toMap() {
    objectMapper.registerModule(new JavaTimeModule());
    return objectMapper.convertValue(this, new TypeReference<>() {});
  }

}
