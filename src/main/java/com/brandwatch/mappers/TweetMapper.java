package com.brandwatch.mappers;

import com.brandwatch.dtos.TweetDTO;
import com.brandwatch.model.Tweet;

import java.util.List;

/**
 * Mapper for tweets
 */
public interface TweetMapper {

  /**
   * Converts tweet DTO to model
   * @param tweetDTO      Tweet DTO
   * @return              Tweet model
   */
  Tweet convertDTOToModel(TweetDTO tweetDTO);

  /**
   * Converts tweet model to DTO
   * @param tweet         Tweet model
   * @return              Tweet DTO
   */
  TweetDTO convertModelToDTO(Tweet tweet);

  /**
   * Converts list of tweet DTOs to models
   * @param tweetDTOs     List of tweet DTOs
   * @return              List of tweet models
   */
  List<Tweet> convertDTOToModel(List<TweetDTO> tweetDTOs);

  /**
   * Converts list of tweet models to DTOs
   * @param tweets        List of tweet models
   * @return              List of tweet DTOs
   */
  List<TweetDTO> convertModelToDTO(List<Tweet> tweets);

}
