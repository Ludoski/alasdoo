package com.brandwatch.mappers.impl;

import com.brandwatch.dtos.TweetDTO;
import com.brandwatch.mappers.TweetMapper;
import com.brandwatch.model.Tweet;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @see TweetMapper
 */
@Service
public class TweetMapperImpl implements TweetMapper {

  /**
   * @see TweetMapper#convertDTOToModel(TweetDTO)
   */
  @Override
  public Tweet convertDTOToModel(TweetDTO tweetDTO) {
    return Tweet.builder()
            .id(tweetDTO.getId())
            .content(tweetDTO.getContent())
            .timestamp(tweetDTO.getTimestamp())
            .build();
  }

  /**
   * @see TweetMapper#convertModelToDTO(Tweet)
   */
  @Override
  public TweetDTO convertModelToDTO(Tweet tweet) {
    return TweetDTO.builder()
            .id(tweet.getId())
            .content(tweet.getContent())
            .timestamp(tweet.getTimestamp())
            .build();
  }

  /**
   * @see TweetMapper#convertDTOToModel(List)
   */
  @Override
  public List<Tweet> convertDTOToModel(List<TweetDTO> tweetDTOs) {
    return tweetDTOs.stream()
            .map(tweetDTO -> Tweet.builder()
                    .id(tweetDTO.getId())
                    .content(tweetDTO.getContent())
                    .timestamp(tweetDTO.getTimestamp())
                    .build())
            .collect(Collectors.toList());
  }

  /**
   * @see TweetMapper#convertModelToDTO(List)
   */
  @Override
  public List<TweetDTO> convertModelToDTO(List<Tweet> tweets) {
    return tweets.stream()
            .map(tweet -> TweetDTO.builder()
                    .id(tweet.getId())
                    .content(tweet.getContent())
                    .timestamp(tweet.getTimestamp())
                    .build())
            .collect(Collectors.toList());
  }

}
